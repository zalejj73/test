export default [
    {id: 1, name: 'Dragon Lore', price: 15300},
    {id: 2, name: 'Wild Lotus', price: 7280},
    {id: 3, name: 'Medusa', price: 6600},
    {id: 4, name: 'Gungnir', price: 9500},
]